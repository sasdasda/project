
-- 登录表 uuid为玩家唯一id
CREATE TABLE IF NOT EXISTS `dtb_user_deviceinfo`(
   `uuid` INT(11) UNSIGNED AUTO_INCREMENT,
   `device_uuid` VARCHAR(100) NOT NULL unique COMMENT "设备唯一id",
   `create_time` int(11) NOT NULL COMMENT "注册时间",
   `last_logintime` int(11) NOT NULL COMMENT "最近一次登录时间",
   `login_type` tinyint(1)  default 0 COMMENT "账号类型 0未绑定 1绑定XX账号..",
   `login_token`VARCHAR(100) NOT NULL COMMENT "登录token",
   PRIMARY KEY ( `uuid`)
)ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8;

-- 游戏列表
CREATE TABLE IF NOT EXISTS `dtb_game_list`(
   `gameid` tinyint(1) default 1  COMMENT "游戏id 1表示目前的后续可扩展",
   `game_url` VARCHAR(100) NOT NULL COMMENT "游戏地址",
   PRIMARY KEY ( `gameid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 玩家主表
CREATE TABLE IF NOT EXISTS `dtb_user_main`(
   `uuid` INT(11) NOT NULL COMMENT "玩家id",
   `user_gold` INT(11)  default 0 COMMENT "金币",
   `user_dimond` int(11) default 0 COMMENT "钻石",
   `user_tili` tinyint(1) default 5 COMMENT "体力",
   `user_data` TEXT   COMMENT "玩家的待保存数据，可以是自定义的字符串",
   `user_name` VARCHAR(200) default "" COMMENT "昵称",
   `head_url` VARCHAR(200) default "" COMMENT "头像url",
   PRIMARY KEY ( `uuid`)
)ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8;

alter table dtb_user_main add syncid int(11) default '0' comment "同步包id";
alter table dtb_user_main add plan_num int(11) default '5' comment "飞机数量";
alter table dtb_user_main add boom_num int(11) default '5' comment "炸弹数量";
alter table dtb_user_main add rainbow_num int(11) default '5' comment "彩虹数量";
alter table dtb_user_main add cur_level int(11) default '100001' comment "当前关卡";


-- 玩家任务表
CREATE TABLE IF NOT EXISTS `dtb_user_task`(
   `uuid` INT(11) NOT NULL COMMENT "玩家id",
   `taskid` INT(11)  NOT NULL COMMENT "任务id",
   `task_state` tinyint(1)  default 0 COMMENT "任务状态",
   `select_item` tinyint(1)  default 0 COMMENT "选择物品",
   `task_startime` int(11) NOT NULL COMMENT "任务开始时间",
   `task_finishtime` int(11) NOT NULL COMMENT "任务结束时间",
   PRIMARY KEY ( `uuid`,`taskid`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- 玩家任务额外表
CREATE TABLE IF NOT EXISTS `dtb_user_extratask`(
   `uuid` INT(11) NOT NULL COMMENT "玩家id",
   `newest_taskid` INT(11)  NOT NULL COMMENT "最近一次完成的任务id",
   `user_data` TEXT   COMMENT "玩家的已经完成的任务列表 用字符串保存 是用逗号分隔的数组",
   PRIMARY KEY ( `uuid`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- 玩家关卡表
CREATE TABLE IF NOT EXISTS `dtb_user_level`(
   `uuid` INT(11) NOT NULL COMMENT "玩家id",
   `battleid` VARCHAR(100)  NOT NULL COMMENT "battleid 唯一id",
   `levelid` INT(11)  NOT NULL COMMENT "关卡id",
   `start_time` int(11) NOT NULL COMMENT "关卡开始时间",
   `finish_time` int(11) NOT NULL COMMENT "关卡结束时间",
   `level_state` tinyint(1)  default 0 COMMENT "任务状态 0表示开始状态 1表示成功 2表示失败",
   PRIMARY KEY ( `uuid`,`battleid`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;