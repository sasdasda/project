package pb

type MessageHeader struct {
	PackageLen uint16
	//DataLen  uint16
	MsgID     uint16
	Seq       uint32
	PlayerID  uint32
	ErrorCode uint32 //回包里面带的错误码
}
