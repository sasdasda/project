module World

go 1.15

require (
	github.com/farmerx/gorsa v0.0.0-20161211100049-3ae06f674f40
	github.com/garyburd/redigo v1.6.2
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2
	github.com/satori/go.uuid v1.2.0
	github.com/wumansgy/goEncrypt v0.0.0-20201114063050-efa0a0601707
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
)
