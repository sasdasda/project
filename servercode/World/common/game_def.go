package common

import (
	"World/common/logger"
	"net"
	"sync"
	"time"
)

type PlayerGameActionType int

const (
	Enum_Game_Action_None PlayerGameActionType = iota
	Enum_Game_Action_ResetAction

	Enum_Game_Action_Check
	Enum_Game_Action_Fold
	Enum_Game_Action_Call
	Enum_Game_Action_Bet
	Enum_Game_Action_Raise
	Enum_Game_Action_Allin

	Enum_Game_Action_Show
	Enum_Game_Action_Muck

	Enum_Game_Action_Sitout
	Enum_Game_Action_Back
)

type SchedAction struct {
	valid  bool
	amount int
	action PlayerGameActionType
}

type GamePlayerData struct {
	UID             int
	Name            string
	Roomid          uint64
	Seat_index      uint32
	Curr_stake      uint32
	Stake_before    uint32
	Sitout          bool
	SitoutStartTime int64 //sitout状态开始时间
	Next_action     SchedAction
	//Last_action     pb.ActionType
	OverLeave bool //这把结束就退出（玩家已经不在了)
	InGaming  bool //玩家是否在游戏中（弃牌，没加入都不是游戏中
	JustIn    bool //刚刚新加入（一圈后再加入，需要post）
}

func (data *GamePlayerData) Init() {
	data.Roomid = 0
	data.Seat_index = 0
}

func (data *GamePlayerData) ResetLastAction() {
	//todo: reset last action
}

type GamePlayer struct {
	UID         int
	Conn        net.Conn
	ConnLocker  *sync.RWMutex //conn设置保护
	OutChannel  chan []byte
	OutDone     chan bool
	ConnSeq     uint32
	ConningSeq  uint32
	Avatar      string
	AvatarThumb string
	LastAt      int64
}

func (player *GamePlayer) UpdateConn(conn net.Conn) {
	if player == nil || player.ConnLocker == nil {
		logger.Error("UpdateConn lockernil")
		return
	}
	player.ConnLocker.Lock()
	defer player.ConnLocker.Unlock()
	player.Conn = conn
}

type GameType int32

const (
	Enum_Game_Type_None         GameType = 0
	Enum_Game_Type_Normal       GameType = 1
	Enum_Game_Type_Club         GameType = 2
	Enum_Game_Type_Special_Club GameType = 3
)

type GameMode int32

const (
	Enum_Game_Mode_None   GameMode = 0
	Enum_Game_Mode_Normal GameMode = 1
	Enum_Game_Mode_Match  GameMode = 2
	Enum_Game_Mode_Other  GameMode = 3
)

type PlayerRemark struct {
	Uid     int32  `json:"uid"`
	Type    int32  `json:"type"`
	Remarks string `json:"remark"`
}

type DtbRoom struct {
	RoomId                  int32     `json:"room_id" xorm:"not null pk autoincr comment('房间id') int32(11)"`
	RoomUuid                uint64    `json:"room_uuid" xorm:"not null pk default 0 comment('房间uuid') BIGINT(20)"`
	RoomName                string    `json:"room_name" xorm:"not null comment('房间名称') VARCHAR(100)"`
	OwnerType               int32     `json:"owner_type" xorm:"comment('1普通牌局/2俱乐部牌局/3定制俱乐部牌局/4联盟牌局') TINYint32(1)"`
	GameMode                int32     `json:"game_mode" xorm:"not null comment('游戏类型 1普通局 3短牌局') TINYint32(1)"`
	RuleBlindEnum           int32     `json:"rule_blind_enum" xorm:"not null comment('大小盲') int32(11)"`
	PlayerCountMax          int32     `json:"player_count_max" xorm:"not null comment('最大玩家数量') int32(11)"`
	RuleTimeLimit           int32     `json:"rule_time_limit" xorm:"not null comment('牌局时间') int32(11)"`
	BuyinMax                int32     `json:"buyin_max" xorm:"not null comment('最大记分牌') int32(11)"`
	BuyinMin                int32     `json:"buyin_min" xorm:"not null comment('最小记分牌') int32(11)"`
	RuleSwitchAntiCheat     int32     `json:"rule_switch_anti_cheat" xorm:"not null comment('IP/GPS限制 1开0关') TINYint32(1)"`
	RuleSwitchRandomSeat    int32     `json:"rule_switch_random_seat" xorm:"not null comment('随机座位开关 1开0关') TINYint32(1)"`
	RuleSwitchForceStraddle int32     `json:"rule_switch_force_straddle" xorm:"not null comment('强制straddle 1开0关') TINYint32(1)"`
	IsAutoDeal              int32     `json:"is_auto_deal" xorm:"not null comment('自动发牌 1开 0关') TINYint32(1)"`
	AutoDealNum             int32     `json:"auto_deal_num" xorm:"not null comment('自动发牌人数') int32(11)"`
	RuleAnteAmount          int32     `json:"rule_ante_amount" xorm:"not null comment('前注金额') int32(11)"`
	IsOpenedDrawback        int32     `json:"is_opened_drawback" xorm:"not null default 0 comment('撤回记分牌按钮 1开启 0关闭') TINYint32(1)"`
	DrawbackHoldTimes       int32     `json:"drawback_hold_times" xorm:"not null comment('撤回记分牌倍数保留的倍数 值*10') int32(11)"`
	DrawbackTimes           int32     `json:"drawback_times" xorm:"not null comment('撤回倍数 值*10') int32(11)"`
	CreateTime              time.Time `json:"create_time" xorm:"not null comment('创建时间') DATETIME"`
	EndTime                 time.Time `json:"end_time" xorm:"not null comment('结束时间') DATETIME"`
	GameTotal               int32     `json:"game_total" xorm:"not null comment('总局数') int32(11)"`
	CreaterId               int32     `json:"creater_id" xorm:"not null comment('创建人ID') int32(11)"`
	ClubId                  int32     `json:"club_id" xorm:"not null comment('俱乐部ID') int(11)"`
	AllianceId              int32     `json:"alliance_id" xorm:"not null comment('联盟ID') int(11)"`
	IsAllianceShare         int32     `json:"is_alliance_share" xorm:"not null comment('是否是联盟共享桌 0-否 1-是') tinyint(1)"`
	AllWaterUps             int32     `json:"all_water_ups" xorm:"not null comment('总水上') int(11)"`
	AntiSimulator           int32     `json:"anti_simulator" xorm:"not null comment('是否允许模拟器 0-否 1-是') tinyint(1)"`
	DrawwaterMode           int32     `json:"drawwater_mode" xorm:"not null default 0 comment('抽水模式 1-把抽 2-局抽') TINYINT(3)"`
	DrawwaterRate           int32     `json:"drawwater_rate" xorm:"not null default 0 comment('抽水比例') INT(11)"`
	DrawwaterMax            int32     `json:"drawwater_max" xorm:"not null default 0 comment('抽水封顶值') INT(11)"`
	AutoCreateId            int32     `json:"auto_create_id" xorm:"not null default 0 comment('自动创建牌桌配置ID') INT(11)"`
	AllianceShareIds        string    `json:"alliance_share_ids" xorm:"default 'NULL' comment('分享联盟ids') TEXT"`
	ClubLimitDown           int32     `json:"club_limit_down" xorm:"not null default 0 comment('同一俱乐部限制坐下人数') INT(8)"`
}

type DtbPlayers struct {
	Id             int32     `json:"id" xorm:"not null pk autoincr int32(11)"`
	GamesId        uint64    `json:"games_id" xorm:"not null comment('游戏id') int32(11)"`
	RoomId         int32     `json:"room_id" xorm:"not null comment('房间id') int32(11)"`
	RoomUuid       uint64    `json:"room_uuid" xorm:"not null default 0 comment('房间uuid') BIGINT(20)"`
	GameNum        int32     `json:"game_num" xorm:"not null comment('第几局游戏') int32(11)"`
	PlayerId       int32     `json:"player_id" xorm:"not null comment('用户id') int32(11)"`
	Cards          string    `json:"cards" xorm:"not null comment('手牌') VARCHAR(100)"`
	TurnInsure     int32     `json:"turn_insure" xorm:"not null comment('转牌购买保险') int32(11)"`
	TurnPayInsure  int32     `json:"turn_pay_insure" xorm:"not null comment('转牌命中保险') int32(11)"`
	SeatNo         int32     `json:"seat_no" xorm:"not null comment('座位号') int32(11)"`
	StartAmount    int32     `json:"start_amount" xorm:"not null comment('开局金额') int32(11)"`
	EndAmount      int32     `json:"end_amount" xorm:"not null comment('结束金额') int32(11)"`
	RiverInsure    int32     `json:"river_insure" xorm:"not null comment('河牌购买保险') int32(11)"`
	RiverPayInsure int32     `json:"river_pay_insure" xorm:"not null comment('河牌命中保险') int32(11)"`
	IsWin          int32     `json:"is_win" xorm:"not null comment('是否赢 1-赢 0-否') TINYINT(1)"`
	AllianceId     int32     `json:"alliance_id" xorm:"not null default 0 comment('联盟ID') int32(11)"`
	ClubId         int32     `json:"club_id" xorm:"not null default 0 comment('俱乐部ID') int32(11)"`
	StartTime      time.Time `json:"start_time" xorm:"default 'NULL' comment('开始时间') DATETIME"`
	EndTime        time.Time `json:"end_time" xorm:"default 'NULL' comment('结束时间') DATETIME"`
	WinBet         int32     `json:"win_bet" xorm:"not null default 0 comment('输赢合计') int32(11)"`
	BuyInsure      int32     `json:"buy_insure" xorm:"not null default 0 comment('购买保险') int32(11)"`
	PayInsure      int32     `json:"pay_insure" xorm:"not null default 0 comment('命中保险') int32(11)"`
}

type DtbGames struct {
	Id             int32     `json:"id" xorm:"not null pk autoincr int32(11)"`
	GamesId        uint64    `json:"games_id" xorm:"not null comment('游戏id') int32(11)"`
	RoomUuid       uint64    `json:"room_uuid" xorm:"not null default 0 comment('房间uuid') BIGINT(20)"`
	RoomId         int32     `json:"room_id" xorm:"not null comment('房间表主键') int32(11)"`
	GameNum        int32     `json:"game_num" xorm:"not null comment('第几局游戏') int32(11)"`
	PublicCards    string    `json:"public_cards" xorm:"not null comment('公共牌') VARCHAR(100)"`
	TurnInsure     int32     `json:"turn_insure" xorm:"not null comment('转牌购买保险') int32(11)"`
	TurnPayInsure  int32     `json:"turn_pay_insure" xorm:"not null comment('转牌购买保险命中') int32(11)"`
	StartTime      time.Time `json:"start_time" xorm:"not null comment('开始时间') DATETIME"`
	EndTime        time.Time `json:"end_time" xorm:"not null comment('结束时间') DATETIME"`
	PlayerTotal    int32     `json:"player_total" xorm:"not null comment('本局人数') int32(11)"`
	RiverInsure    int32     `json:"river_insure" xorm:"comment('河牌购买保险') int32(11)"`
	RiverPayInsure int32     `json:"river_pay_insure" xorm:"comment('河牌命中保险') int32(11)"`
	AllianceId     int32     `json:"alliance_id" xorm:"not null default 0 comment('联盟ID') int32(11)"`
	ClubId         int32     `json:"club_id" xorm:"not null default 0 comment('俱乐部ID') int32(11)"`
}

type DtbGame struct {
	Id        int32     `json:"id" xorm:"not null pk autoincr INT(11)"`
	Gameuuid  string    `json:"gameuuid" xorm:"not null comment('mongodb game_hand表gameuuid') VARCHAR(50)"`
	Playeruid int32     `json:"playeruid" xorm:"not null default 0 comment('玩家id') INT(11)"`
	RoomUuid  uint64    `json:"room_uuid" xorm:"not null default 0 comment('房间uuid') BIGINT(20)"`
	Roomid    int32     `json:"roomid" xorm:"not null default 0 comment('房间id') INT(11)"`
	RoomName  string    `json:"room_name" xorm:"not null default '' comment('房间名称') VARCHAR(50)"`
	Endtime   time.Time `json:"endtime" xorm:"comment('结束时间') DATETIME"`
	Handcount int32     `json:"handcount" xorm:"not null default 0 comment('总手数') INT(11)"`
	Level     int32     `json:"level" xorm:"not null default 0 comment('盲注级别') INT(11)"`
	Ante      int32     `json:"ante" xorm:"not null comment('前注') INT(11)"`
}

type DtbUserMain struct {
	Id               int    `json:"id" xorm:"not null pk autoincr comment('用户ID') INT(11)"`
	Areacode         string `json:"areaCode" xorm:"default '' VARCHAR(10)"`
	Mobile           string `json:"mobile" xorm:"default '' comment('手机号') index VARCHAR(20)"`
	Passwd           string `json:"passwd" xorm:"default '' comment('密码') CHAR(32)"`
	Avatar           string `json:"avatar" xorm:"default '' comment('用户头像') VARCHAR(255)"`
	AvatarThumb      string `json:"avatar_thumb" xorm:"default '' comment('头像(来自APP端) 缩略图') VARCHAR(255)"`
	NickName         string `json:"nick_name" xorm:"default '' comment('昵称') VARCHAR(40)"`
	Gender           int    `json:"gender" xorm:"default 1 comment('性别 0未定义/保密 1男性 2女性') TINYINT(1)"`
	UserMarks        string `json:"user_marks" xorm:"default '' comment('个性签名') VARCHAR(60)"`
	UserArea         string `json:"user_area" xorm:"default '' comment('所在地区') VARCHAR(20)"`
	DiamondNum       int    `json:"diamond_num" xorm:"default 0 comment('钻石数量') INT(11)"`
	UserGold         int    `json:"user_gold" xorm:"default 0 comment('用户金币数量') INT(11)"`
	GamesMax         int    `json:"games_max" xorm:"default 0 comment('可组建的牌局数量') INT(11)"`
	ClubsMax         int    `json:"clubs_max" xorm:"default 0 comment('可创建的俱乐部数量') INT(11)"`
	CurrentGames     int    `json:"current_games" xorm:"default 0 comment('当前创建的牌局数量') INT(11)"`
	CurrentClubs     int    `json:"current_clubs" xorm:"default 0 comment('当前创建的俱乐部数量') INT(11)"`
	Point            int    `json:"point" xorm:"default 0 comment('用户积分') INT(11)"`
	RegCode          string `json:"reg_code" xorm:"default '' comment('邀请注册码') unique VARCHAR(8)"`
	SaleCode         string `json:"sale_code" xorm:"default '' comment('折扣推广码') unique VARCHAR(8)"`
	CardType         int    `json:"card_type" xorm:"default 0 comment('卡片类型 0未定义 1蓝卡，2金卡(月卡)，3白金卡(年卡)') TINYINT(1)"`
	CardExpire       int    `json:"card_expire" xorm:"default 0 comment('会员卡过期时间') INT(11)"`
	Token            string `json:"token" xorm:"default '' comment('令牌标识') VARCHAR(200)"`
	DeviceType       int    `json:"device_type" xorm:"default 0 comment('设备类型：0 未知类型 1 ios 2 Android') TINYINT(1)"`
	RegTime          int    `json:"reg_time" xorm:"default 0 comment('注册时间') INT(10)"`
	LastLogin        int    `json:"last_login" xorm:"default 0 comment('最后登录时间') INT(10)"`
	IsOnline         int    `json:"is_online" xorm:"default 0 comment('是否在线 0 离线  1在线') TINYINT(1)"`
	RoomId           int    `json:"room_id" xorm:"not null default 0 comment('当前的房间ID') INT(20)"`
	VgUid            string `json:"vg_uid" xorm:"comment('绑定vg钱包注册uid') VARCHAR(200)"`
	Uid              int    `json:"uid" xorm:"comment('用户注册uid') INT(11)"`
	Status           int    `json:"status" xorm:"default 0 comment('用户状态：0正常，1禁用') TINYINT(1)"`
	IsAllowSmallGame int    `json:"is_allow_small_game" xorm:"default 0 comment('是否允许开启小金额牌桌 0不允许 1允许') TINYINT(1)"`
	Ip               string `json:"ip" xorm:"default '' comment('用户IP地址') VARCHAR(30)"`
	Device           string `json:"device" xorm:"default '' comment('用户设备') VARCHAR(50)"`
}

type WfDbAudit struct {
	Id              int32     `json:"id" xorm:"not null pk autoincr comment('自增ID') int32(11)"`
	RoomUuid        int64     `json:"room_uuid" xorm:"not null default 0 comment('id') BIGINT(20)"`
	GameNum         int32     `json:"game_num" xorm:"not null default 0 comment('第几局游戏') int32(11)"`
	RoomId          int32     `json:"room_id" xorm:"not null default 0 comment('房间id') int32(11)"`
	RoomName        string    `json:"room_name" xorm:"not null comment('房间名称') VARCHAR(100)"`
	OwnerType       int32     `json:"owner_type" xorm:"not null default 0 comment('1普通牌局/2俱乐部牌局/3定制俱乐部牌局/4联盟牌局') TINYINT(1)"`
	IsAllianceShare int32     `json:"is_alliance_share" xorm:"not null default 0 comment('是否是联盟共享桌 0-否 1-是') TINYINT(1)"`
	CreateTime      time.Time `json:"create_time" xorm:"not null default 'current_timestamp()' comment('创建时间') DATETIME"`
	CardsDetail     string    `json:"cards_detail" xorm:"default 'NULL' comment('发牌详情') TEXT"`
}

type RoundCard struct {
	FlopCards    string                     `json:"flop_cards"`
	TurnCards    string                     `json:"turn_cards"`    //转牌
	RivierCards  string                     `json:"rivier_cards"`  //河牌
	RoundPlayers map[int32]*RoundCardPlayer `json:"round_players"` //用户详情
}

type RoundCardPlayer struct {
	PlayerId   int32  `json:"player_id"`   //用户id
	PlayerName string `json:"player_name"` //用户昵称
	HoldCards  string `json:"hold_cards"`  //手牌
	Ip         string `json:"ip"`          //ip addr
}
