package redis

import (
	"World/common/logger"
	"encoding/json"
	"strconv"
	"time"
)

type TestRedisSS struct {
	Uuid        int32
	User_star   int32 //星星
	User_dimond int32 //美钞
	User_tili   int32 //体力
	Plan_num    int32 //飞机数量
	Boom_num    int32 //飞机数量
	Rainbow_num int32 //飞机数量
	Cur_level   int32 //飞机数量
	Sync_id     int64 //同步包id
}

func TestRedis() {
	var tmp TestRedisSS
	tmp.Uuid = 1
	tmp.User_star = 2
	tmp.User_dimond = 3
	tmp.User_tili = 4
	tmp.Plan_num = 5
	tmp.Boom_num = 6
	tmp.Rainbow_num = 7
	tmp.Cur_level = 8
	tmp.Sync_id = 9

	val, _ := json.Marshal(&tmp)
	time1 := time.Now().Nanosecond() / 1000000
	for i := 0; i < 10000; i++ {
		key := "testredis" + strconv.Itoa(i)

		GetRedisClient().HSet("testredisssss", key, string(val))
	}

	time2 := time.Now().Nanosecond() / 1000000
	diff := time2 - time1
	logger.Info("TestRedis  ssss time=%v time1=%v time2=%v", diff, time1, time2)
}
