package redis

import (
	"World/common/logger"
	"World/common/redis"
	"World/conf"
	"errors"
)

var gRedis *redis.RedisClient = nil

func TestMyredis() {
	logger.Info("TestMyredis")
	err := gRedis.HSet("luheng", "luhengsb", "22")
	if err != nil {
		logger.Error("TestMyredis failed err=%v", err)
	}
	gRedis.HSet("luheng", "luhengbb", "33")
	go TestRedis()
}

func GetRedisIsOpen() bool {
	rs, err := gRedis.GetString(WD_REDIS_FLAG)
	if err != nil {
		return false
	}
	if rs != "1" {
		return false
	}
	return true
}

func GetRedisClient() *redis.RedisClient {
	return gRedis
}

func Init() error {
	redis_cfg := conf.GetRedisConf()
	if gRedis = redis.NewRedisClient(redis_cfg.Host, redis_cfg.Password, redis_cfg.Db); gRedis == nil {
		return errors.New("initRedis error")
	}

	TestMyredis()
	return nil
}
