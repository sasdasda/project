package rpc

import (
	"World/common/logger"
	rpc_world "World/pb/rpc_world"

	"golang.org/x/net/context"
)

type server struct{}

func (s *server) CreateRoom(ctx context.Context, in *rpc_world.CreateRoomRequest) (*rpc_world.CreateRoomReply, error) {
	var r rpc_world.CreateRoomReply
	logger.Info("CreateRoom rep=%+v", r)
	r.Message = "test success"
	r.ProfitScale = 10
	return &r, nil
}
