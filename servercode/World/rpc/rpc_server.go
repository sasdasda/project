package rpc

import (
	"World/common/logger"
	"World/conf"
	rpc_world "World/pb/rpc_world"
	"google.golang.org/grpc"
	"net"
)

func DoGRPC() {
	logger.Info("***DoGRPC***")
	addr := conf.GetRpcListenAddr()
	//	lis, err := net.Listen("tcp", port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Fatal("failed to listen: %v", err)
	} else {
		logger.Info("DoGRPC listen success addr=%+v", addr)
	}
	logger.Info("DoCommunicate mid")
	s := grpc.NewServer()
	rpc_world.RegisterRoomServer(s, &server{})
	err = s.Serve(lis)
	if err != nil {
		logger.Fatal("failed to serve: %v", err)
	} else {
		logger.Info("DoCommunicate server success!")
	}
	logger.Info("DoCommunicate end")
}
