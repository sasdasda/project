package network

import (
	_ "World/conf"
	_ "World/pb"
	_ "errors"
	_ "fmt"

	_ "World/libs/glog"
	_ "github.com/golang/protobuf/proto"
	_ "golang.org/x/net/context"
	"google.golang.org/grpc"
)

var (
	cf_rpc  *grpc.ClientConn
	cf_addr = "localhost:50051"
)

func InitRPC() error {
	var err error
	cf_rpc, err = grpc.Dial(cf_addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	return err
}

func DoRPC() {
	//todo: 参数，返回值细化
}
