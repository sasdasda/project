package db

import (
	"World/common/logger"
)

func SyncUserBaseInfo(info *UserBaseInfo) error {
	if info == nil {
		return nil
	}
	cmd := "insert into dtb_user_main(uuid,user_gold,user_dimond,user_tili,syncid,plan_num,boom_num,rainbow_num,cur_level) " +
		"values(?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE user_gold = user_gold + 0 "
	_, err := m_game_db.Query(cmd, info.Uuid, info.User_star, info.User_dimond, info.User_tili, info.Sync_id, info.Plan_num, info.Boom_num, info.Rainbow_num, info.Cur_level)
	if err != nil {
		logger.Error("SyncUserBaseInfo err=%v", err)
		return err
	}
	return nil
}

func SyncUserDeviceInfo(info *UserDeviceInfo) error {
	if info == nil {
		return nil
	}

	cnt := -1
	row := m_game_db.QueryRow("select count(*) from dtb_user_deviceinfo where uuid = ?", info.Uuid)

	err := row.Scan(&cnt)
	if err != nil {
		logger.Error("SyncUserLevelInfo err=%v", err)
		return err
	}

	if cnt != -1 {
		cmd := "update dtb_user_deviceinfo set create_time = ?,last_logintime = ?,login_type=?,login_token = ?  where uuid = ? "
		_, err := m_game_db.Query(cmd, info.Create_time, info.Last_logintime, info.Login_type, info.Token, info.Uuid)
		if err != nil {
			logger.Error("SyncUserLevelInfo err=%v", err)
			return err
		}
	} else {
		cmd := "insert into dtb_user_deviceinfo(uuid,device_uuid,create_time,last_logintime,login_type,login_token) values(?,?,?,?,?,?)"
		_, err := m_game_db.Query(cmd, info.Uuid, info.Device_uuid, info.Create_time, info.Last_logintime, info.Login_type, info.Token)
		if err != nil {
			logger.Error("SyncUserSignInfo err=%v", err)
			return err
		}
	}

	return nil
}
