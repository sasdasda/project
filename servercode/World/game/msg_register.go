package game

import (
	"World/network"
	pb "World/pb"
)

func Init() {

	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_World_Logon_Req), &pb.WorldLogonReq{}, LoginHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_WORLD_ConnClose_Nty), &pb.WorldNoticeConnectionClose{}, ConnCloseHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_World_HeartBeat_Req), &pb.WorldHeartBeatReq{}, HeartBeatHandler)

}
