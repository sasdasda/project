package main

import (
	"GameServer/pb"
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/golang/protobuf/proto"
	"log"
	"net"
)

func LevelStart(conn net.Conn, uuid int) {
	var req pb.GameLevelStartReq
	//var dataLen int

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_CS_Game_QuestGetAll_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func DoSign(conn net.Conn, uuid int) {
	var req pb.GameDoSignReq
	req.BaseInfo = new(pb.GameOfflineBaseInfo)
	//var dataLen int
	req.BaseInfo.Syncid = 43

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_CS_Game_DoSign_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func FetchTaskGruop(conn net.Conn, uuid int) {
	var req pb.GameTaskGroupRewardReq
	req.BaseInfo = new(pb.GameOfflineBaseInfo)
	//var dataLen int
	req.BaseInfo.Syncid = 47
	req.TaskId = 1

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_CS_Game_TaskGroupReward_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func NewGuideFin(conn net.Conn, uuid int) {
	var req pb.GameNewGuideFinReq
	req.BaseInfo = new(pb.GameOfflineBaseInfo)
	req.Info = new(pb.NewGuideInfo)
	req.BaseInfo.Syncid = 11
	req.Info.GuideId = 1001
	req.Info.State = 1
	req.Info.Type = 2

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_CS_Game_NewGuideFin_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func ContinueStep(conn net.Conn, uuid int) {
	var req pb.GameContinueReq
	req.BaseInfo = new(pb.GameOfflineBaseInfo)
	req.BaseInfo.Syncid = 12
	req.Cnt = 1
	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_CS_Game_Continue_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func GetNewGuideData(conn net.Conn, uuid int) {
	var req pb.GameGetNewGuideDataReq

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_SC_Game_GetNewGuideData_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}

func GetSignData(conn net.Conn, uuid int) {
	var req pb.GameGetSignDataReq
	//var dataLen int

	tmp, err := proto.Marshal(&req)
	if err != nil {
		fmt.Println("marshal proto failed:", err.Error())
		return
	}

	//dataLen = len(tmp)

	body_len := len(tmp)
	total_len := body_len + Min_Message_Size
	bys := new(bytes.Buffer)
	seq := 1

	uid := uuid

	rid := 0
	//binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(total_len))
	binary.Write(bys, binary.BigEndian, uint16(pb.MSGID_MsgID_SC_Game_GetSignData_Req))
	//binary.Write(bys, binary.BigEndian, uint16(12))
	binary.Write(bys, binary.BigEndian, uint32(seq))
	binary.Write(bys, binary.BigEndian, uint32(uid))
	binary.Write(bys, binary.BigEndian, uint32(rid))
	fmt.Println("create club body total:", body_len)
	buf := append(bys.Bytes(), tmp...)
	fmt.Println("Login len:", len(buf))

	conn.Write(buf)
	if err != nil {
		log.Println("||||||||||||||||=================ssssb err=%v", err)
	}
}
