module GameServer

go 1.16

require (
	github.com/farmerx/gorsa v0.0.0-20161211100049-3ae06f674f40
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gin-gonic/gin v1.7.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.4.2
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	google.golang.org/grpc v1.37.0
)
