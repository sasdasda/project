package game

import "math/big"

func Fib(i int) int {
	if i <= 0 {
		return 0
	}
	if i == 1 {
		return 1
	}
	if i == 2 {
		return 1
	}
	return Fib(i-1) + Fib(i-2)
}

func Fib2(n int) int {
	big.NewInt(2)
	var fibarry = []int{0, 1, 0}
	for i := 2; i < n; i++ {
		fibarry[2] = fibarry[0] + fibarry[1]
		fibarry[0] = fibarry[1]
		fibarry[1] = fibarry[2]
	}
	return fibarry[2]
}

func Test() {
	//this func is for test
	Fib(4)
}
