package game

import (
	"GameServer/network"
	pb "GameServer/pb"
)

func Init() {

	//GM协议
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_GmModifty_Req), &pb.GameGmModiftyReq{}, GameGmModiftyHandler)

	//正常协议
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_Logon_Req), &pb.GameLogonReq{}, LoginHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_Game_ConnClosed_Nty), &pb.GameNoticeConnectionClose{}, ConnCloseHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_HeartBeat_Req), &pb.GameHeartBeatReq{}, HeartBeatHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_QuestGetAll_Req), &pb.GameQuestGetAllReq{}, QuestGetAllHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_SC_Game_GetSignData_Req), &pb.GameGetSignDataReq{}, GetSignDataReqHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_SC_Game_GetSignData_Req), &pb.GameGetSignDataReq{}, GetSignDataReqHandler)
	network.RegisterMessage(uint16(pb.MSGID_MsgID_SC_Game_GetNewGuideData_Req), &pb.GameGetNewGuideDataReq{}, GetNewGuideReqHandler)

	//离线验证协议
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_QuestFinishOffline_Req), &pb.GameQuestFinishOfflineReq{}, QuestFinishOfflineHandler) //任务完成上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_QuestChangeItem_Req), &pb.GameQuestChangeItemReq{}, QuestChangeItemHandler)          //改变任务选择的物品
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_LevelStart_Req), &pb.GameLevelStartReq{}, LevelStartHandler)                         //关卡开始上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_LevelEnd_Req), &pb.GameLevelEndReq{}, LevelEndHandler)                               //关卡结束上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_DoSign_Req), &pb.GameDoSignReq{}, DoSignHandler)                                     //关卡结束上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_TaskGroupReward_Req), &pb.GameTaskGroupRewardReq{}, TaskGroupRewardHandler)          //关卡结束上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_NewGuideFin_Req), &pb.GameNewGuideFinReq{}, NewGuideFinReqHandler)                   //关卡结束上报
	network.RegisterMessage(uint16(pb.MSGID_MsgID_CS_Game_Continue_Req), &pb.GameContinueReq{}, ContinueReqHandler)                            //续步请求
}
