package utils

import (
	"GameServer/common/logger"
	stat "GameServer/common/statistic"
	"GameServer/conf"
	"fmt"
	"runtime"
	"time"

	"GameServer/libs/go-spew/spew"
)

// 产生panic时的调用栈打印
func PrintPanicStack(extras ...interface{}) {
	if x := recover(); x != nil {
		logger.Info("panic recover stack : %+v", x)
		i := 0
		funcName, file, line, ok := runtime.Caller(i)
		for ok {
			logger.Info("frame %v:[func:%v,file:%v,line:%v]\n", i, runtime.FuncForPC(funcName).Name(), file, line)
			i++
			funcName, file, line, ok = runtime.Caller(i)
		}

		for k := range extras {
			logger.Info("EXRAS#%v DATA:%v\n", k, spew.Sdump(extras[k]))
		}
	}
}

func SendCurrencyReport(uid uint32, clubid int32, change_channel, currency_type uint32, change_amount, param1, param2, param3 int, desc string) {
	var report stat.CurrencyReport
	report.ChangeTime = uint32(time.Now().Unix())
	report.SourceType = conf.GetSourceType()
	report.Uid = uid
	if clubid > 0 {
		report.ClubId = uint32(clubid)
	}
	report.ChangeChannel = change_channel
	report.CurrencyType = currency_type
	report.ChangeAmount = change_amount
	report.Param1 = param1
	report.Param2 = param2
	report.Param3 = param3
	report.Desc = desc
	cfg := conf.GetEventReportHttpAddrConf()
	stat.UploadReport(cfg.Host+cfg.CurrencyUri, &report)
}

func SendEventReport(uid uint32, clubid int32, event_type uint32, param1, param2, param3 int, desc string) {
	var report stat.EventReport
	report.EventTime = uint32(time.Now().Unix())
	report.SourceType = conf.GetSourceType()
	report.Uid = uid
	if clubid > 0 {
		report.ClubId = uint32(clubid)
	}
	report.EventType = event_type
	report.Param1 = param1
	report.Param2 = param2
	report.Param3 = param3
	report.Desc = desc
	cfg := conf.GetEventReportHttpAddrConf()
	stat.UploadReport(cfg.Host+cfg.EventUri, &report)
}

func SendFairPlayReport(reporter_id, rid uint32, clubid int32, room_uuid, game_uuid uint64, uids []uint32, contact, detail string) {
	var report stat.FairPlayReport
	report.EventTime = uint32(time.Now().Unix())
	report.SourceType = conf.GetSourceType()
	report.Uid = reporter_id
	if clubid > 0 {
		report.ClubId = uint32(clubid)
	}
	report.RoomId = rid
	report.RoomUuid = fmt.Sprintf("%+v", room_uuid)
	report.GameUuid = fmt.Sprintf("%+v", game_uuid)
	report.SuspectIds = getStringOfSlice(uids)
	report.Contact = contact
	report.Detail = detail
	report.Status = 0

	cfg := conf.GetEventReportHttpAddrConf()
	stat.UploadReport(cfg.Host+cfg.FairPlayUri, &report)

}

func getStringOfSlice(ids []uint32) string {
	s := ""
	for _, id := range ids {
		s += fmt.Sprintf("%d,", id)
	}
	return s
}

func SendDeviceInfoReport(uid uint32, host string, channel uint32, device_info string) {
	var report stat.DeviceInfoRecord
	report.EventTime = uint32(time.Now().Unix())
	report.SourceType = conf.GetSourceType()
	report.Uid = uid
	report.Channel = channel
	report.Host = host
	report.DeviceInfo = device_info
	cfg := conf.GetEventReportHttpAddrConf()
	stat.UploadReport(cfg.Host+cfg.DeviceInfoUri, &report)
}
