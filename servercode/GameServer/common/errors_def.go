package common

import (
	"errors"
)

var (
	ErrorsNotFoundPlayerInMysql = errors.New("not found uid in mysql")
	ErrorsShortOfMoney          = errors.New("short of money")
)

type ErrorType int

const (
	Error_OK                                        = 0     //成功
	Error_NIL                                       = 1     //空的，暂时没有用处
	Error_Player_Not_Found                ErrorType = 40001 //玩家未能找到
	Error_Op_Db_Failed                    ErrorType = 40002 //服务器内部错误
	Error_Check_Token_Failed              ErrorType = 40003 //token验证失败
	Error_Check_Syncid_Failed             ErrorType = 40004 //同步id太大
	Error_Check_PreQuest_notFin           ErrorType = 40005 //前置任务还未完成
	Error_Check_StarNum_notEnough         ErrorType = 40006 //任务所需星星数不足
	Error_Check_Syncid_AlreadyHandled     ErrorType = 40007 //同步id太小 当前协议已经处理过了
	Error_Check_Cfg_NotFound              ErrorType = 40008 //配置文件没有找到
	Error_Check_Pre_levelNotFin           ErrorType = 40009 //前置关卡尚未完成
	Error_Check_Tili_NotEnough            ErrorType = 40010 //体力不足
	Error_Check_Item_NotEnough            ErrorType = 40011 //道具不足
	Error_Check_Level_NotStart            ErrorType = 40012 //没有关卡开始的记录或者关卡已经完成
	Error_Check_Is_Signed                 ErrorType = 40013 //已经签到过了
	Error_Check_Sign_Reward_Failed        ErrorType = 40014 //发放签到奖励失败
	Error_Check_TaskGroup_Reward_Failed   ErrorType = 40015 //发放任务组奖励失败
	Error_Check_TaskGroup_Already_Fetched ErrorType = 40016 //奖励任务已结领取过了
	Error_Check_TaskGroup_NotFin          ErrorType = 40017 //奖励任务组奖励没有完成
	Error_Check_USD_NotEnough             ErrorType = 40018 //美钞不足

)
