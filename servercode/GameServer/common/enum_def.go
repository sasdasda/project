package common

const (
	Enum_Max_Player_Count        = 10
	Enum_Player_Max              = 9
	Enum_Action_Time             = 15
	Enum_Max_Noti_Observer_Count = 40
	Enum_SitOut_Time_Seconds     = 180
	Min_Message_Size             = 16
)

const (
	Enum_GM_Optype_CLeanData    = 1 //清除数据
	Enum_GM_Optype_OpearteStar  = 2 //操作星星数
	Enum_GM_Optype_OpearteTili  = 3 //操作体力
	Enum_GM_Optype_OpearteUSD   = 4 //操作美钞
	Enum_GM_Optype_OpearteItem  = 5 //操作道具
	Enum_GM_Optype_OpearteLevel = 6 //操作跳关

)

const (
	Enum_Item_USD     = 1 //美钞
	Enum_Item_Tili    = 2 //体力
	Enum_Item_Coin    = 3 //金币
	Enum_Item_Dimond  = 4 //金币
	Enum_Item_Ticket  = 5 //金币
	Enum_Item_Plane   = 6 //飞机
	Enum_Item_Boom    = 7 //炸弹
	Enum_Item_RainBow = 8 //彩虹

)

const (
	Enum_Apply_Alliance_Agree  = 1
	Enum_Apply_Alliance_Refuse = 2
)

const (
	Enum_Up_To_Club_Admin    = 0
	Enum_Down_To_Club_Normal = 1
	Enum_Remove_Club_Member  = 2
)

const (
	Enum_Club_Level_one   = 1
	Enum_Club_Level_two   = 2
	Enum_Club_Level_three = 3
	Enum_Club_Level_four  = 4
	Enum_Club_Level_five  = 5
	Enum_Club_Level_six   = 6
	Enum_Club_Level_seven = 7
	Enum_Club_Level_eight = 8
)

const (
	Enum_Club_Public    = 0
	Enum_Club_Private   = 1
	Enum_Club_Recommend = 2
)

const (
	Enum_In_Club     = 1
	Enum_Not_In_Club = 2
)

//用户状态
const (
	Enum_PlayerState_Leave        = -1 //离开游戏
	Enum_PlayerState_Wait         = 0  //等待中
	Enum_PlayerState_Gaming       = 1  //游戏中
	Enum_PlayerState_StayPosition = 2  //保卫离桌
	Enum_PlayerState_SettleLeave  = 3  //结算离桌
	Enum_PlayerState_Attended     = 4  //已参与
)
