package db

import (
	"GameServer/common"
	"GameServer/common/logger"
	"GameServer/conf"
	"database/sql"
	"time"
	_ "time"

	_ "github.com/go-sql-driver/mysql"
)

var (
	m_game_db *sql.DB
	//	m_active_db             *sql.DB
	club_create_tablename = "dtb_club_create_info"
	user_tablename        = "dtb_user_main"
	activity_tablename    = "dtb_activity_record"
	gameround_tablename   = "dtb_table_round"
	remarks_tablename     = "dtb_remarks"
)

func InitMysql() error {
	db, err := InitMysqlByConf(conf.GetGameDBConf())
	if err != nil {
		return err
	}
	db.SetConnMaxLifetime(240 * time.Second)
	db.SetMaxOpenConns(50)
	db.SetMaxIdleConns(20)
	m_game_db = db

	err = m_game_db.Ping()
	if err != nil {
		logger.Error("InitMysql failed err=%v", err)
		return err
	}
	return nil
}

func InitMysqlByConf(cfg conf.MysqlConf) (*sql.DB, error) {
	url := cfg.User + ":" + cfg.Pwd + "@tcp(" + cfg.Ip + ":" + common.Itoa(cfg.Port) + ")/" + cfg.Database + "?charset=utf8&loc=Local"
	db, err := sql.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	return db, nil
}

/*
func InitMysqlActive() error {
	db, err := InitMysqlActiveByConf(conf.GetActiveDBConf())
	if err != nil {
		return err
	}
	m_active_db = db
	return nil
}

func InitMysqlActiveByConf(cfg conf.MysqlConf) (*sql.DB, error) {
	url := cfg.User + ":" + cfg.Pwd + "@tcp(" + cfg.Ip + ":" + common.Itoa(cfg.Port) + ")/" + cfg.Database + "?charset=utf8&loc=Local"
	db, err := sql.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	return db, nil
}
*/
