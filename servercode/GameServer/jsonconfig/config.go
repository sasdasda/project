package jconf

import (
	"GameServer/common/logger"
	"encoding/json"
	"io/ioutil"
)

var (
	Tabtask = NewTable()
)

func Init() {
	LoadTaskConf()
}

// 重新加载指定文件名的表
func ReloadTable(filename string) {

	// 根据需要从你的源数据读取，这里从指定文件名的文件读取
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		logger.Error("ReloadTable failed er=%v", err)
		return
	}

	// 重置数据，这里会触发Prehandler
	Tabtask.ResetData()

	// 使用json反序列化
	err = json.Unmarshal(data, Tabtask)

	if err != nil {
		logger.Error("ReloadTable failed er=%v", err)
		return
	}

	// 构建数据和索引，这里会触发PostHandler
	Tabtask.BuildData()

	info := GetTaskGruopInfo(1)
	if info != nil {
		logger.Info("test taskinfo =%+v", *info)
	}
}

func LoadTaskConf() {
	logger.Info("LoadTaskConf begin!")
	Tabtask.RegisterPostEntry(func(tab *Table) error {
		logger.Info("LoadTaskConf %+v\n", tab.TaskDataInfo[0].TaskID)

		return nil
	})

	ReloadTable("../jsonconfig/json_task.json")

}

func GetTaskDataInfo(taskid int32) *TaskDataInfo {
	info, ok := Tabtask.TaskDataInfoByTaskID[taskid]
	if !ok {
		return nil
	}
	return info
}

func GetMapSeqInfo(seqid int32) *MapSequenceInfo {
	for _, v := range Tabtask.MapSequenceInfo {
		if v.ID == seqid {
			return v
		}
	}
	return nil
}

func GetMapSeqInfoByMapID(levelid int32) *MapSequenceInfo {
	info, ok := Tabtask.MapSequenceInfoByLevelID[levelid]
	if !ok {
		return nil
	}
	return info
}

func DumpMapSequenceInfoByID() {
	for k, v := range Tabtask.MapSequenceInfo {
		logger.Info("DumpMapSequenceInfoByID k=%v,v=%v", k, *v)
	}

	for k, v := range Tabtask.MapSequenceInfoByID {
		logger.Info("=====DumpMapSequenceInfoByID k=%v,v=%v", k, *v)
	}
}

func GetSignDataInfoByDay(signday int32) *SignInfo {
	for _, v := range Tabtask.SignInfo {
		if v.SignDay == signday {
			return v
		}
	}
	return nil
}

func GetTaskGroupReward(taskid int32) *RewardGroup {
	info, ok := Tabtask.RewardGroupByTaskID[taskid]
	if !ok {
		return nil
	}
	return info
}

func GetTaskGruopInfo(unqiueid int32) *TaskGroupInfo {
	info, ok := Tabtask.TaskGroupInfoByUniqueID[unqiueid]
	if !ok {
		return nil
	}
	return info
}

func GetContinueStep(stepid int32) *ContinueStep {
	info, ok := Tabtask.ContinueStepByConID[stepid]
	if !ok {
		maxindex := 0
		maxid := int32(-1)
		//获取最大的配置
		for k, val := range Tabtask.ContinueStep {
			if maxid < val.ConID {
				maxid = val.ConID
				maxindex = k
			}
		}

		if maxindex < len(Tabtask.ContinueStep) {
			return Tabtask.ContinueStep[maxindex]
		}

	}
	return info
}
