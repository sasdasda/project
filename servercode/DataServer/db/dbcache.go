package db

import (
	"DataServer/common"
	"DataServer/conf"
	"database/sql"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"sync"
)

var (
	m_db_gamedb *sql.DB
	m_locker    = new(sync.Mutex)

	ErrorsNotFoundPlayerInMysql = errors.New("not found uid in mysql")
)

func init() {

}

func InitMysqlByConf(cfg conf.MysqlConf) (*sql.DB, error) {
	url := cfg.User + ":" + cfg.Pwd + "@tcp(" + cfg.Ip + ":" + common.Itoa(cfg.Port) + ")/" + cfg.Database + "?charset=utf8&loc=Local"
	db, err := sql.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func InitMysql() error {

	db, err := InitMysqlByConf(conf.GetGameDBConf())
	if err != nil {
		return err
	}
	m_db_gamedb = db
	return nil
}
