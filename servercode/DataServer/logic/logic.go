package logic

import (
	"DataServer/common/logger"
	"DataServer/conf"
	rpc_ds "DataServer/rpc_ds"
	context "golang.org/x/net/context"
	"google.golang.org/grpc"
	"net"
)

type PaipuService struct{}

func (s *PaipuService) CreateRoom(ctx context.Context, in *rpc_ds.CreateRoomRequest) (*rpc_ds.CreateRoomReply, error) {
	//检查房间是否存在，构造存储数据库中的数据结构
	res := HandleCreateRoom(in)
	return &rpc_ds.CreateRoomReply{Message: "sb", ProfitScale: int32(res)}, nil
}

func InitRPCService() {
	addr := conf.GetRPCListenAddrConf()
	if len(addr) == 0 {
		logger.Info(" no rpc listen addr, not to start rpc service")
		return
	}
	listen, err := net.Listen("tcp", addr)
	s := grpc.NewServer()

	rpc_ds.RegisterDataSeviceServer(s, &PaipuService{})
	if err = s.Serve(listen); err != nil {
		logger.Info(" failed to start rpc service at:%v error:%v", addr, err)
	}
}
