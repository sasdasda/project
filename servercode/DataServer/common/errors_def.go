package common

import (
	"errors"
)

var (
	ErrorsNotFoundPlayerInMysql = errors.New("not found uid in mysql")
	ErrorsShortOfMoney          = errors.New("short of money")
)

type ErrorType int

const (
	Error_OK                         = 0     //成功
	Error_NIL                        = 1     //空的暂时没有用
	Error_Reg_Failed                 = 30001 //注册失败
	Error_Player_Not_Found ErrorType = 30002 //玩家未能找到
	Error_Op_Db_Failed     ErrorType = 30003 //服务器内部错误

)
