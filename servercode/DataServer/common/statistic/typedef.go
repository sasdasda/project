package statistic

const (
	Currency_Channel_SendCardFun        = 1
	Currency_Channel_InteractiveExpress = 2
	Currency_Channel_Buyin              = 3
	Currency_Channel_Buyout             = 4
	Currency_Channel_ReturnLeftStake    = 5
	Currency_Channel_SyncAlliance       = 6
	Currency_Channel_AddActionTime      = 7
	Currency_Channel_AddInsuranceTime   = 8
	Currency_Channel_AddRoomTime        = 9

	Currency_Channel_RechargeClubFund = 10
	Currency_Channel_BuyClubLevel     = 11

	Currency_Channel_WebMarket_Apple  = 12 //apple pay channel
	Currency_Channel_WebMarket_Weixin = 13 //weixin pay channel
	Currency_Channel_WebMarket_Other  = 14 //other pay channel
	Currency_Channel_WebMarket        = 15 //web market buy
	Currency_Channel_User_Regist      = 16 //user regist

	Currency_Channel_GrantClubFund = 17
	Currency_Channel_Admin_Modify = 18 //后台修改
	Currency_Channel_ForceShowCard = 19
	Currency_Channel_ReturnGold = 20	//宕机恢复玩家的金币
	
	Currency_Type_Diamond = 1
	Currency_Type_Money   = 2 //就是Gold

	PayTool_Type_Club     = 1
	PayTool_Type_Platform = 2

	Server_Type_World = 1
	Server_Type_Game  = 2
	Server_Type_Data  = 3
	Server_Type_MTT   = 4

	Event_Type_Login               = 1
	Event_Type_CreateRoom          = 2
	Event_Type_GameEnd             = 3
	Event_Type_ModifyBuyinLimit    = 4
	Event_Type_Answer_Buyin        = 5
	Event_Type_InsuranceHappen     = 6
	Event_Type_BuyInsurance        = 7
	Event_Type_ProhibitSitDown     = 8
	Event_Type_ForceStandup        = 9
	Event_Type_CheckOutAndLeave    = 10
	Event_Type_WorldLogin          = 11
	Event_Type_GameHand_Force      = 12
	Event_Type_GameServerOnline    = 13
	Event_Type_User_Regist         = 14
	Event_Type_JackpotInjectAmount = 15
	Event_Type_Insurance_Cheat     = 16
	Event_Type_Jackpot_Awardinfo   = 17
	Event_Type_Shot_Insurance      = 18
	Event_Type_FairPlay      	   = 19
)

//货币report
type CurrencyReport struct {
	Id            int
	ChangeTime    uint32
	SourceType    uint32
	OrderId       string
	Uid           uint32
	ClubId        uint32
	RoomId        uint32
	RoleName      string
	ChangeChannel uint32
	CurrencyType  uint32
	ChangeAmount  int
	Param1        int
	Param2        int
	Param3        int
	Desc          string
}

type EventReport struct {
	Id         int
	EventTime  uint32
	SourceType uint32
	Uid        uint32
	RoleName   string
	RoomId     uint32
	ClubId     uint32
	EventType  uint32
	Param1     int
	Param2     int
	Param3     int
	Desc       string
	Desc2      string
}
//
type ToolPayReport struct {
	Id          int
	PayTime     uint32
	SourceType  uint32
	PayToolType uint32
	MoneyType   uint32
	Mobile      string
	Amount      uint32
	Desc        string
}

//伙牌举报记录
type FairPlayReport struct {
	Id         int
	EventTime  uint32
	SourceType uint32
	Uid        uint32
	RoleName   string
	RoomId     uint32
	ClubId     uint32
	RoomUuid   string  //举报手牌所在房间的唯一ID
	GameUuid   string  //举报手牌唯一id
	SuspectIds string  //嫌疑人uids
	Contact    string  //反馈联系方式
	Detail     string  //伙牌举报描述
	Status     uint32  //处理状态(默认0，待细化)
	Param1     uint32  //冗余int参数1
	Param2     uint32  //冗余int参数2
	Desc       string  //冗余字符串参数1
	Desc2      string  //冗余字符串参数2
	Desc3      string  //冗余字符串参数3
	Desc4      string  //冗余字符串参数4
}

//游戏手牌记录信息（待伙牌审查）
type FairPlayGameHandRecord struct {
	Id         int
	EventTime  uint32
	SourceType uint32
	RoomId     uint32
	ClubId     uint32
	RoomUuid   string  //举报手牌所在房间的唯一ID
	GameUuid   string  //举报手牌唯一id
	Uids 	   string  //这手牌的参与玩家uid列表
	GameMode   uint32  //游戏模式
	Blind      uint32  //BB大小
	Ante       uint32  //ante大小
	HandNo     uint32  //这个房间第几手牌
	HandInfoHead    string   `orm:"size(2048)"` //牌局信息（头）
	HoleCardsDetail string   `orm:"size(2048)"` //手牌信息（详细）
	HoleCardsShow   string   `orm:"size(2048)"` //手牌信息（旁观视角）
	HandInfoTail    string   `orm:"size(2048)"` //牌局信息（尾）
	SeatInfo   string   `orm:"size(512)"`//座位玩家信息
}

//设备信息记录
type DeviceInfoRecord struct {
	Id         int
	EventTime  uint32
	SourceType uint32
	Uid        uint32
	Channel    uint32  //1:注册；2:充值；3:登录
	Host       string   `orm:"size(512)"` //链接的客户端ip信息
	DeviceInfo string   `orm:"size(4196)"` //用户设备信息
}
