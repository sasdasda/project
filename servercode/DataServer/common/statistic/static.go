package statistic
import(
	"bytes"
	"encoding/json"
 	"io/ioutil"
	"net/http"
)

func UploadReport(url string, report interface{}){
	go upload(url, report)
}
	
func upload(url string, report interface{}){
	bys, err := json.Marshal(report)
	if err != nil {
 		return
	}
	body := bytes.NewBuffer(bys)
	res, err := http.Post(url, "application/json;charset=utf-8", body)
	if err != nil {
		return
	}
	_, _ = ioutil.ReadAll(res.Body)
	res.Body.Close()
}














