// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// source code is here : https://github.com/astaxie/beego/blob/master/utils/safemap.go

//package utils
package beegomap

import (
	"sync"
)

// BeeMap is a map with lock
type BeeMap struct {
	lock *sync.RWMutex
	Bm   map[uint32]interface{}
}

// NewBeeMap return new safemap
func NewBeeMap() *BeeMap {
	return &BeeMap{
		lock: new(sync.RWMutex),
		Bm:   make(map[uint32]interface{}),
	}
}

// Get from maps return the k's value
func (m *BeeMap) Get(k uint32) interface{} {
	m.lock.RLock()
	defer m.lock.RUnlock()
	if val, ok := m.Bm[k]; ok {
		return val
	}
	return nil
}

// Set Maps the given key and value. Returns false
// if the key is already in the map and changes nothing.
func (m *BeeMap) Set(k uint32, v interface{}) bool {
	m.lock.Lock()
	defer m.lock.Unlock()
	if val, ok := m.Bm[k]; !ok {
		m.Bm[k] = v
	} else if val != v {
		m.Bm[k] = v
	} else {
		return false
	}
	return true
}

//force set
func (m *BeeMap) Replace(k uint32, v interface{}) bool {
	m.lock.Lock()
	defer m.lock.Unlock()
	delete(m.Bm, k)
	if val, ok := m.Bm[k]; !ok {
		m.Bm[k] = v
	} else if val != v {
		m.Bm[k] = v
	} else {
		return false
	}
	return true
}

// Check Returns true if k is exist in the map.
func (m *BeeMap) Check(k uint32) bool {
	m.lock.RLock()
	defer m.lock.RUnlock()
	if _, ok := m.Bm[k]; !ok {
		return false
	}
	return true
}

// Delete the given key and value.
func (m *BeeMap) Delete(k uint32) {
	m.lock.Lock()
	defer m.lock.Unlock()
	delete(m.Bm, k)
}

// Items returns all items in safemap.
func (m *BeeMap) Items() map[uint32]interface{} {
	m.lock.RLock()
	defer m.lock.RUnlock()
	r := make(map[uint32]interface{})
	for k, v := range m.Bm {
		r[k] = v
	}
	return r
}

func (m *BeeMap) Len() uint32 {
	m.lock.RLock()
	defer m.lock.RUnlock()
	return uint32(len(m.Bm))
}

func (m *BeeMap)Clear(){
	items := m.Items()
	for k := range items{
		m.Delete(k)
	}
}
func (m *BeeMap)GetRandomKey()uint32{
	for k := range m.Items() {
			return k
	}
	return 0
}
