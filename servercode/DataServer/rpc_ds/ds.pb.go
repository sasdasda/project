// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ds.proto

/*
Package __rpc_ds is a generated protocol buffer package.

It is generated from these files:
	ds.proto

It has these top-level messages:
	CreateRoomRequest
	CreateRoomReply
*/
package __rpc_ds

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// 创建房间时通知请求参数
type CreateRoomRequest struct {
	CreatePlayerId int32  `protobuf:"varint,1,opt,name=create_player_id,json=createPlayerId" json:"create_player_id,omitempty"`
	CreatorName    string `protobuf:"bytes,2,opt,name=creator_name,json=creatorName" json:"creator_name,omitempty"`
}

func (m *CreateRoomRequest) Reset()                    { *m = CreateRoomRequest{} }
func (m *CreateRoomRequest) String() string            { return proto.CompactTextString(m) }
func (*CreateRoomRequest) ProtoMessage()               {}
func (*CreateRoomRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *CreateRoomRequest) GetCreatePlayerId() int32 {
	if m != nil {
		return m.CreatePlayerId
	}
	return 0
}

func (m *CreateRoomRequest) GetCreatorName() string {
	if m != nil {
		return m.CreatorName
	}
	return ""
}

// 创建房间时通知返回参数
type CreateRoomReply struct {
	Message     string `protobuf:"bytes,1,opt,name=message" json:"message,omitempty"`
	ProfitScale int32  `protobuf:"varint,2,opt,name=profit_scale,json=profitScale" json:"profit_scale,omitempty"`
}

func (m *CreateRoomReply) Reset()                    { *m = CreateRoomReply{} }
func (m *CreateRoomReply) String() string            { return proto.CompactTextString(m) }
func (*CreateRoomReply) ProtoMessage()               {}
func (*CreateRoomReply) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *CreateRoomReply) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *CreateRoomReply) GetProfitScale() int32 {
	if m != nil {
		return m.ProfitScale
	}
	return 0
}

func init() {
	proto.RegisterType((*CreateRoomRequest)(nil), "rpc_ds.CreateRoomRequest")
	proto.RegisterType((*CreateRoomReply)(nil), "rpc_ds.CreateRoomReply")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for DataSevice service

type DataSeviceClient interface {
	// 创建房间时通知
	CreateRoom(ctx context.Context, in *CreateRoomRequest, opts ...grpc.CallOption) (*CreateRoomReply, error)
}

type dataSeviceClient struct {
	cc *grpc.ClientConn
}

func NewDataSeviceClient(cc *grpc.ClientConn) DataSeviceClient {
	return &dataSeviceClient{cc}
}

func (c *dataSeviceClient) CreateRoom(ctx context.Context, in *CreateRoomRequest, opts ...grpc.CallOption) (*CreateRoomReply, error) {
	out := new(CreateRoomReply)
	err := grpc.Invoke(ctx, "/rpc_ds.DataSevice/CreateRoom", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for DataSevice service

type DataSeviceServer interface {
	// 创建房间时通知
	CreateRoom(context.Context, *CreateRoomRequest) (*CreateRoomReply, error)
}

func RegisterDataSeviceServer(s *grpc.Server, srv DataSeviceServer) {
	s.RegisterService(&_DataSevice_serviceDesc, srv)
}

func _DataSevice_CreateRoom_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRoomRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DataSeviceServer).CreateRoom(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc_ds.DataSevice/CreateRoom",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DataSeviceServer).CreateRoom(ctx, req.(*CreateRoomRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DataSevice_serviceDesc = grpc.ServiceDesc{
	ServiceName: "rpc_ds.DataSevice",
	HandlerType: (*DataSeviceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateRoom",
			Handler:    _DataSevice_CreateRoom_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "ds.proto",
}

func init() { proto.RegisterFile("ds.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 218 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0xd0, 0xbf, 0x4f, 0xc4, 0x20,
	0x14, 0xc0, 0x71, 0x6b, 0x72, 0xe7, 0xdd, 0x3b, 0xe3, 0x0f, 0x16, 0xab, 0xd3, 0xd9, 0xa9, 0x13,
	0x83, 0x8e, 0x6e, 0xa7, 0x8b, 0x4b, 0xd3, 0xd0, 0xcd, 0x05, 0x9f, 0xf0, 0x34, 0x4d, 0x40, 0x10,
	0xd0, 0xa4, 0xff, 0xbd, 0x29, 0x68, 0x34, 0xd1, 0x91, 0xcf, 0x4b, 0xbe, 0xe4, 0x3d, 0x58, 0xe9,
	0xc8, 0x7d, 0x70, 0xc9, 0xb1, 0x65, 0xf0, 0x4a, 0xea, 0xd8, 0x3c, 0xc2, 0xe9, 0x6d, 0x20, 0x4c,
	0x24, 0x9c, 0xb3, 0x82, 0xde, 0xde, 0x29, 0x26, 0xd6, 0xc2, 0x89, 0xca, 0x28, 0xbd, 0xc1, 0x89,
	0x82, 0x1c, 0x75, 0x5d, 0x6d, 0xab, 0x76, 0x21, 0x8e, 0x8a, 0xf7, 0x99, 0xef, 0x35, 0xbb, 0x84,
	0xc3, 0x2c, 0x2e, 0xc8, 0x57, 0xb4, 0x54, 0xef, 0x6f, 0xab, 0x76, 0x2d, 0x36, 0x5f, 0xd6, 0xa1,
	0xa5, 0xa6, 0x83, 0xe3, 0xdf, 0x3f, 0x78, 0x33, 0xb1, 0x1a, 0x0e, 0x2c, 0xc5, 0x88, 0x2f, 0x94,
	0xb3, 0x6b, 0xf1, 0xfd, 0x9c, 0x7b, 0x3e, 0xb8, 0xe7, 0x31, 0xc9, 0xa8, 0xd0, 0x94, 0xde, 0x42,
	0x6c, 0x8a, 0x0d, 0x33, 0x5d, 0xf5, 0x00, 0x77, 0x98, 0x70, 0xa0, 0x8f, 0x51, 0x11, 0xdb, 0x01,
	0xfc, 0xd4, 0xd9, 0x39, 0x2f, 0x6b, 0xf1, 0x3f, 0x3b, 0x5d, 0x9c, 0xfd, 0x37, 0xf2, 0x66, 0x6a,
	0xf6, 0x76, 0xf0, 0xb0, 0xe2, 0x37, 0x65, 0xfa, 0xb4, 0xcc, 0xe7, 0xb9, 0xfe, 0x0c, 0x00, 0x00,
	0xff, 0xff, 0xab, 0x02, 0xd9, 0xfa, 0x2a, 0x01, 0x00, 0x00,
}
