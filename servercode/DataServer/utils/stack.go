package utils

import (
	"DataServer/common/logger"

	"fmt"
	"runtime"

	"DataServer/libs/go-spew/spew"
)

// 产生panic时的调用栈打印
func PrintPanicStack(extras ...interface{}) {
	if x := recover(); x != nil {
		logger.Info("panic recover stack : %+v", x)
		i := 0
		funcName, file, line, ok := runtime.Caller(i)
		for ok {
			logger.Info("frame %v:[func:%v,file:%v,line:%v]\n", i, runtime.FuncForPC(funcName).Name(), file, line)
			i++
			funcName, file, line, ok = runtime.Caller(i)
		}

		for k := range extras {
			logger.Info("EXRAS#%v DATA:%v\n", k, spew.Sdump(extras[k]))
		}
	}
}

func getStringOfSlice(ids []uint32) string {
	s := ""
	for _, id := range ids {
		s += fmt.Sprintf("%d,", id)
	}
	return s
}
