package commdef

type ItemBase_SimpleDropItem struct {
	ItemId           int32 `protobuf:"zigzag32,1,req,name=item_id,json=itemId" json:"item_id,omitempty"`
	ItemCnt          int32 `protobuf:"zigzag32,2,req,name=item_cnt,json=itemCnt" json:"item_cnt,omitempty"`

}

type ItemBase struct {
	Id               int32                     `protobuf:"zigzag32,1,req,name=id" json:"id,omitempty"`
	Name             string                    `protobuf:"bytes,2,req,name=name" json:"name,omitempty"`
	ExpireTime       int32                     `protobuf:"zigzag32,3,req,name=expire_time,json=expireTime" json:"expire_time,omitempty"`
	MaxPileCnt       int32                     `protobuf:"zigzag32,4,req,name=max_pile_cnt,json=maxPileCnt" json:"max_pile_cnt,omitempty"`
	MaxCnt           int32                     `protobuf:"zigzag32,5,req,name=max_cnt,json=maxCnt" json:"max_cnt,omitempty"`
	UseImmediately   int32                     `protobuf:"zigzag32,6,req,name=use_immediately,json=useImmediately" json:"use_immediately,omitempty"`
	CanBuy           bool                      `protobuf:"varint,7,req,name=can_buy,json=canBuy" json:"can_buy,omitempty"`
	BuyOffline       bool                      `protobuf:"varint,8,req,name=buy_offline,json=buyOffline" json:"buy_offline,omitempty"`
	BuffId           int32                     `protobuf:"zigzag32,9,req,name=buff_id,json=buffId" json:"buff_id,omitempty"`
	SimpleDrops      []*ItemBase_SimpleDropItem `protobuf:"bytes,10,rep,name=simple_drops,json=simpleDrops" json:"simple_drops,omitempty"`
	BuildingId       int32                     `protobuf:"zigzag32,11,req,name=building_id,json=buildingId" json:"building_id,omitempty"`
	ItemType         int32                     `protobuf:"zigzag32,12,req,name=item_type,json=itemType" json:"item_type,omitempty"`
	ItemExtents      []int32                    `protobuf:"zigzag32,13,rep,name=item_extents,json=itemExtents" json:"item_extents,omitempty"`
	ItemDes          string                    `protobuf:"bytes,14,req,name=item_des,json=itemDes" json:"item_des,omitempty"`
	ItemCanuseinbag  bool                      `protobuf:"varint,15,req,name=item_canuseinbag,json=itemCanuseinbag" json:"item_canuseinbag,omitempty"`
	CostMoney        int32                     `protobuf:"zigzag32,16,req,name=cost_money,json=costMoney" json:"cost_money,omitempty"`
	CostDiamond      int32                     `protobuf:"zigzag32,17,req,name=cost_diamond,json=costDiamond" json:"cost_diamond,omitempty"`
	UseOffline       bool                      `protobuf:"varint,18,req,name=use_offline,json=useOffline" json:"use_offline,omitempty"`
	UnlockLevel      int32                     `protobuf:"zigzag32,19,req,name=unlock_level,json=unlockLevel" json:"unlock_level,omitempty"`
	UseAudio         int32                     `protobuf:"zigzag32,20,req,name=use_audio,json=useAudio" json:"use_audio,omitempty"`
	CanSeeInBag      bool                      `protobuf:"varint,21,req,name=can_see_in_bag,json=canSeeInBag" json:"can_see_in_bag,omitempty"`
	Icon             string                    `protobuf:"bytes,22,req,name=icon" json:"icon,omitempty"`
	ShortDes         string                    `protobuf:"bytes,23,req,name=short_des,json=shortDes" json:"short_des,omitempty"`
	NewbieMapid      int32                     `protobuf:"zigzag32,24,req,name=newbie_mapid,json=newbieMapid" json:"newbie_mapid,omitempty"`
}


type ItemBase_Tbl struct {
	Crc32            int64      `protobuf:"zigzag64,1,req,name=crc32" json:"crc32,omitempty"`
	Items            []*ItemBase `protobuf:"bytes,6,rep,name=items" json:"items,omitempty"`
}


// excel: ../item.xlsx
// sheet: 开局道具
type StartBattleItemBase struct {
	Id               int32  `protobuf:"zigzag32,1,req,name=id" json:"id,omitempty"`
	Name             string `protobuf:"bytes,2,req,name=name" json:"name,omitempty"`
}

type StartBattleItemBase_Tbl struct {
	Crc32            int64                 `protobuf:"zigzag64,1,req,name=crc32" json:"crc32,omitempty"`
	Items            []*StartBattleItemBase `protobuf:"bytes,6,rep,name=items" json:"items,omitempty"`
}


// excel: ../item.xlsx
// sheet: 限时道具映射
type TimeItemMapBase struct {
	Id               int32  `protobuf:"zigzag32,1,req,name=id" json:"id,omitempty"`
	MapIds           []int32 `protobuf:"zigzag32,2,rep,name=map_ids,json=mapIds" json:"map_ids,omitempty"`
}

type TimeItemMapBase_Tbl struct {
	Crc32            int64             `protobuf:"zigzag64,1,req,name=crc32" json:"crc32,omitempty"`
	Items            []*TimeItemMapBase `protobuf:"bytes,6,rep,name=items" json:"items,omitempty"`
}