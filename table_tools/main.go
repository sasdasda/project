package main

import (
	"MyTest/logic"
	"encoding/json"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)



func read() []byte{
	var filePath string = "./TableGarden.bytes"
	fp, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}
	defer fp.Close()

	buff := make([]byte, 102400)

	for {
		len, err := fp.Read(buff)
		if err == io.EOF || len < 0 {
			break
		}

		fmt.Print(string(buff[:len]))
	}
	return buff
}

type TableGarden_Tbl struct {
	Crc32            int64         `protobuf:"zigzag64,1,req,name=crc32" json:"crc32,omitempty"`
	Items            []*TableGarden `protobuf:"bytes,2,rep,name=items" json:"items,omitempty"`
}

type Boundary struct {
	X                float32 `protobuf:"fixed32,1,req,name=x" json:"x,omitempty"`
	Y                float32 `protobuf:"fixed32,2,req,name=y" json:"y,omitempty"`
	Z                float32 `protobuf:"fixed32,3,req,name=z" json:"z,omitempty"`
}

type TableGarden struct {
	Id               int32      `protobuf:"zigzag32,1,req,name=id" json:"id,omitempty"`
	Name             string     `protobuf:"bytes,2,req,name=name" json:"name,omitempty"`
	StaticGardenRes  string     `protobuf:"bytes,3,req,name=staticGardenRes" json:"staticGardenRes,omitempty"`
	CharacterRes     string     `protobuf:"bytes,4,req,name=characterRes" json:"characterRes,omitempty"`
	LutTexture       string     `protobuf:"bytes,5,req,name=lutTexture" json:"lutTexture,omitempty"`
	LutBlendTexture  string     `protobuf:"bytes,6,req,name=lutBlendTexture" json:"lutBlendTexture,omitempty"`
	Boundary         []*Boundary `protobuf:"bytes,7,rep,name=boundary" json:"boundary,omitempty"`
	RegionCount      int32      `protobuf:"zigzag32,8,req,name=regionCount" json:"regionCount,omitempty"`
	Audios           []int32     `protobuf:"zigzag32,9,rep,name=audios" json:"audios,omitempty"`
}


func readExcel(excelPath string) ([]string,*TableGarden_Tbl){
	var restt TableGarden_Tbl
	xlsx,err:=excelize.OpenFile(excelPath)
	if err !=nil{
		fmt.Println("open excel error,",err.Error())
		os.Exit(1)
	}

	rows:=xlsx.GetRows(xlsx.GetSheetName(xlsx.GetActiveSheetIndex()))
	//rows:=xlsx.GetRows(xlsx.GetSheetName(xlsx.GetSheetIndex("区域表")))
	result:= make([]string,0)
	for k,row:=range rows{
		if k > 1 {
			if len(row)==11 {
				var tmp TableGarden
				id,_ := strconv.Atoi(row[0])
				tmp.Id = int32(id)
				tmp.Name = row[1]
				tmp.StaticGardenRes = row[2]
				tmp.CharacterRes = row[3]
				tmp.LutTexture = row[4]
				tmp.LutBlendTexture = row[5]
				recnt,_ := strconv.Atoi(row[7])
				tmp.RegionCount = int32(recnt)
				a := strings.Split(row[6], ";")
				for _,va := range a {
					var vBoundary Boundary
					vvva := strings.Split(va, ",")
					if len(vvva)!=3 {
						continue
					}
					tmpx,_ :=strconv.ParseFloat(vvva[0],32)
					tmpy,_ :=strconv.ParseFloat(vvva[1],32)
					tmpz,_ :=strconv.ParseFloat(vvva[2],32)
					vBoundary.X = float32((tmpx))
					vBoundary.Y = float32((tmpy))
					vBoundary.Z = float32((tmpz))
					tmp.Boundary = append(tmp.Boundary,&vBoundary)
				}

				b := strings.Split(row[8], ";")
				for _,vb := range b {
					tmpid,_ := strconv.Atoi(vb)
					tmp.Audios = append(tmp.Audios,int32(tmpid))
				}


				result = append(result,row[0])
				restt.Items = append(restt.Items,&tmp)
			}



		}

	}
	return result,&restt
}

func main () {
	/*tmp :=read()
	t := tbl.TableGarden_Tbl{}

	err := proto.Unmarshal(tmp, &t)
	if err != nil {
		log.Println("failed err=",err)
	}*/
	//proto.Unmarshal(tmp, tbl.TableGarden)

	ss,res :=readExcel("./花园表.xlsx")
	for _,val:=range ss {
		log.Println(val)
	}


	for _,val := range res.Items {
		fmt.Println("final res=%+v",*val)
	}

	binary,_ :=json.Marshal(&res)
	fmt.Println("ss=%s",string(binary))

	f, err1 := os.Create("./garden.json") //创建文件
	if err1 != nil {
		fmt.Println("打开文件失败,err=%v",err1)
	}
	_, err1 = io.WriteString(f, string(binary)) //写入文件(字符串)
	if err1 != nil {
		fmt.Println("写入文件失败,err=%v",err1)
	}

	logic.ReadExcelItem()
}