package logic

import (
	"MyTest/commdef"
	"encoding/json"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"io"
	"os"
	"strconv"
	"strings"
)

func ReadExcelItem() {
	var restt commdef.ItemBase_Tbl
	xlsx,err:=excelize.OpenFile("./xlxs/item.xlsx")
	if err !=nil{
		fmt.Println("readExcelItem open excel error,",err.Error())
		os.Exit(1)
	}

	//rows:=xlsx.GetRows(xlsx.GetSheetName(xlsx.GetActiveSheetIndex()))
	rows:=xlsx.GetRows(xlsx.GetSheetName(xlsx.GetSheetIndex("基础道具表")))
	//result:= make([]string,0)
	for k,row:=range rows{
		if k > 1 {
			if len(row)==30 {
				var tmp commdef.ItemBase
				id,_ := strconv.Atoi(row[0])
				if id == 0 {
					continue
				}
				tmp.Id = int32(id)
				tmp.Name = row[1]
				expire,_ := strconv.Atoi(row[3])
				tmp.ExpireTime = int32(expire)
				maxpile,_ := strconv.Atoi(row[9])
				tmp.MaxPileCnt = int32(maxpile)
				maxcnt,_ := strconv.Atoi(row[10])
				tmp.MaxCnt = int32(maxcnt)
				isuseimmadle,_ := strconv.Atoi(row[11])
				tmp.UseImmediately = int32(isuseimmadle)
				canbuy := false
				if row[5] == "true" {
					canbuy = true
				}
				tmp.CanBuy = canbuy
				buyof := false
				if row[6] == "true" {
					buyof = true
				}
				tmp.BuyOffline = buyof
				buffid,_ := strconv.Atoi(row[12])
				tmp.BuffId = int32(buffid)

				//简单掉落
				a := strings.Split(row[15], ";")
				for _,va := range a {
					var vBoundary commdef.ItemBase_SimpleDropItem
					vvva := strings.Split(va, ",")
					if len(vvva)!=2 {
						continue
					}
					tmpx,_ :=strconv.Atoi(vvva[0])
					tmpy,_ :=strconv.Atoi(vvva[1])

					vBoundary.ItemId = int32((tmpx))
					vBoundary.ItemCnt = int32((tmpy))

					tmp.SimpleDrops = append(tmp.SimpleDrops,&vBoundary)
				}

				buildid,_ := strconv.Atoi(row[16])
				tmp.BuildingId = int32(buildid)

				itemtype,_ := strconv.Atoi(row[17])
				tmp.ItemType = int32(itemtype)


				b := strings.Split(row[18], ";")
				for _,vb := range b {
					tmpid,_ := strconv.Atoi(vb)
					tmp.ItemExtents = append(tmp.ItemExtents,int32(tmpid))
				}

				tmp.ItemDes = row[19]
				itemcanuseinbag := false
				if row[20] == "true" {
					itemcanuseinbag = true
				}
				tmp.ItemCanuseinbag = itemcanuseinbag

				gold,_ := strconv.Atoi(row[8])
				tmp.CostMoney = int32(gold)
				dimond,_ := strconv.Atoi(row[9])
				tmp.CostDiamond = int32(dimond)

				useoffline := false
				if row[21] == "true" {
					useoffline = true
				}
				tmp.UseOffline = useoffline

				unlocklv,_ := strconv.Atoi(row[22])
				tmp.UnlockLevel = int32(unlocklv)

				useaudio,_ := strconv.Atoi(row[23])
				tmp.UseAudio = int32(useaudio)

				canseeinbag := false
				if row[24] == "true" {
					canseeinbag = true
				}
				tmp.CanSeeInBag = canseeinbag

				tmp.Icon = row[25]
				tmp.ShortDes = row[2]
				newmapid,_ := strconv.Atoi(row[29])
				tmp.NewbieMapid = int32(newmapid)

				restt.Items = append(restt.Items,&tmp)
			}



		}

	}
	//保存到json
	binary,_ :=json.Marshal(&restt)
	fmt.Println("ss=%s",string(binary))

	f, err1 := os.Create("./output/item.json") //创建文件
	if err1 != nil {
		fmt.Println("打开文件失败,err=%v",err1)
	}
	_, err1 = io.WriteString(f, string(binary)) //写入文件(字符串)
	if err1 != nil {
		fmt.Println("写入文件失败,err=%v",err1)
	}

	fmt.Println("readExcelItem 完成！")
}
