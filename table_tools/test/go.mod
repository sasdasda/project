module configtool

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.2.0
	github.com/BurntSushi/toml v0.3.1
	github.com/akavel/rsrc v0.10.2 // indirect
	github.com/lxn/walk v0.0.0-20200729152207-1bbd02fe6a9f
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
	golang.org/x/sys v0.0.0-20200805065543-0cf7623e9dbd // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
