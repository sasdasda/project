package parser

const tpl = `package {{.PkgName}}
{{range .List}}
type {{.Name}}List struct {
	{{.Name}} []*{{.Name}}
}

type {{.Name}} struct { {{range .Field}}
	{{.Name}}	{{NameTypeFunc .}}  {{if .IsAnonymStruct}} { {{range .AnonymStruct.Field}}
			 {{.Name}} {{NameTypeFunc .}} 	// {{.NameType}} {{.Comment}}    {{end}}
	} {{else}} // {{.NameType}} {{.Comment}} {{end}} {{end}}
}
{{end}}
`

// 客户端模板
const cstplH = `
using ETModel;

namespace {{.ClientNameSpace}}
{
	[Config((int)({{.ClientNick}}))]
	public partial class T{{.Name}}AVOCategory : ACategory<T{{.Name}}AVO>
	{
	}
	{{range .List}}
	public class T{{.Name}}AVO: IConfig 
	{ {{range .Field}}
		/// <summary>{{.Comment}}</summary>
		{{NameTypeClientFunc .}}
		{{end}}
	}
	{{end}}


}
`

// 客户端枚举模板 EConfigValue 和 TSystemLanguage
const cstplSysLang = `namespace ETHotfix
{
	public enum {{.PkgName}} 
	{
		{{.Name}} 
	}
}
`

// 服务器ConfigValue
const tplConfigValue = `package common

const (
		{{.Name}} 
)
`
