// 自定义函数
package main

import (
	"configtool/parser"
	"os"
	"time"

	"github.com/lxn/walk"
	"github.com/pquerna/ffjson/ffjson"
)

var excel, pkg, codeOutputPath, dataOutputPath, version string // 文件路径，导出go文件的包名，输出代码路径，输出json路径，语言版本

// 导出文件
func (t *Tool) export() {
	if msg := parser.Export(t.textConfig.Text(), version, t.textExprot.Text()); msg != "" {
		t.log(msg)
	} else {
		// 更新配置地址
		t.updateConfig()
		walk.MsgBox(mw, "成功", "导出成功！", walk.MsgBoxOK)
	}
}

// 版本选择
func (t *Tool) selectVersion() {
	switch t.comboBox.CurrentIndex() {
	case 0: // dev
		version = "dev"
		t.log("选择版本:dev")
	case 1: // zh_CN
		version = "zh_CN"
		t.log("选择版本:zh_CN")
	}
}

// 记录日志
func (t *Tool) log(msg string) {
	msg = "[" + time.Now().Format("2006-01-02 15:04:05") + "] " + msg
	t.textLog.AppendText(msg + "\r\n")
}

// 更新配置地址
func (t *Tool) updateConfig() {
	if t.textExprot.Text() != "" {
		Cfg.Server.SvnPath = t.textExprot.Text()
		b, err := ffjson.Marshal(Cfg)
		if err != nil {
			t.log(err.Error())
			return
		}

		writeFile, err := os.OpenFile("./conf.json", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
		defer writeFile.Close()
		if err != nil {
			t.log(err.Error())
			return
		}
		_, err = writeFile.Write(b)
		if err != nil {
			t.log(err.Error())
			return
		}

	}
}
