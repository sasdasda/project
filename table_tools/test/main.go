package main

import (
	"strings"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

var tool Tool

type Tool struct {
	textConfig *walk.TextEdit // 配置文件路径
	comboBox   *walk.ComboBox // 版本选择
	textExprot *walk.TextEdit // 导出文件路径
	textLog    *walk.TextEdit // 日志
}

var mw *MyMainWindow

type MyMainWindow struct {
	*walk.MainWindow
}

func main() {

	// 初始化配置
	InitConfig()

	mw = new(MyMainWindow)

	MainWindow{
		AssignTo: &mw.MainWindow,
		Title:    "导表小工具",
		Icon:     "icon.ico", //窗体图标
		MinSize:  Size{Width: 600, Height: 400},
		Size:     Size{Width: 800, Height: 600},
		Layout:   VBox{},
		OnDropFiles: func(files []string) {
			tool.textConfig.SetText(strings.Join(files, "\r\n"))
		},
		Children: []Widget{
			HSplitter{
				Children: []Widget{
					Label{Text: "版本"},
					ComboBox{
						AssignTo:              &tool.comboBox,
						Model:                 []string{"dev", "zh_CN"},
						OnCurrentIndexChanged: tool.selectVersion,
					},
				},
			},
			HSplitter{
				Children: []Widget{
					Label{Text: "配置"},
					TextEdit{
						AssignTo: &tool.textConfig,
						ReadOnly: true,
						Text:     "配置文件拖到这里...",
					},
				},
			},
			HSplitter{
				Children: []Widget{
					Label{Text: "地址"},
					TextEdit{
						AssignTo: &tool.textExprot,
						Text:     Cfg.Server.SvnPath,
					},
				},
			},
			HSplitter{
				Children: []Widget{
					Label{Text: "日志"},
					TextEdit{
						AssignTo: &tool.textLog,
						ReadOnly: true,
						Text:     "",
						VScroll:  true,
					},
				},
			},
			PushButton{
				Text:      "导出",
				OnClicked: tool.export,
			},
		},
	}.Run()

}
