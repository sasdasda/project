package main

import (
	"flag"
	"io/ioutil"

	"github.com/pquerna/ffjson/ffjson"
)

var (
	cfgFile = flag.String("ConfigCfg", "conf.json", "log config file") // 文件路径
)

// 总配置
type jsonConfig struct {
	Title  string
	Server server
}

// 服务器配置
type server struct {
	SvnPath string // svn_path
}

// Cfg 全局程序配置
var Cfg *jsonConfig

// 加载配置文件
func InitConfig() {

	Cfg = new(jsonConfig)
	data, err := ioutil.ReadFile(*cfgFile)
	if err != nil {
		panic(err)
	}
	err = ffjson.Unmarshal(data, Cfg)
	if err != nil {
		panic(err)
	}

}
