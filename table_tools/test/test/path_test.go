package test

import (
	"encoding/json"
	"fmt"
	"path/filepath"
	"strings"
	"testing"

	"github.com/pquerna/ffjson/ffjson"
)

//总配置
type tomlConfig struct {
	Title  string
	Server server
}

//服务器配置
type server struct {
	SvnPath string // svn_path
}

func TestHello(t *testing.T) {
	src := `d:\workspace\go\excel_to_file\movieaction.json`
	fmt.Println(filepath.Base(src))

	// Cfg 全局程序配置
	var Cfg = new(tomlConfig)
	Cfg.Server.SvnPath = `d:\workspace\go\excel_to_file`
	b, _ := json.Marshal(Cfg)
	fmt.Println(string(b))

}

func TestJsonPS(t *testing.T) {
	str := `{"Id":20010001,"type":1,"position":"[19,1.02,19]","furnitureId":-1,"faceId":0,"actionId":10010003,"NextPositionId":20010002}`
	var s = make(map[string]interface{})
	s["Id"] = 20010001
	s["type"] = 1
	s["position"] = "[19,1.02,19]"
	s["furnitureId"] = -1
	s["faceId"] = 0
	s["actionId"] = 10010003
	s["NextPositionId"] = 20010002
	err := ffjson.Unmarshal([]byte(str), &s)
	if err != nil {
		fmt.Println(err)
	}

	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(b))

	smap := &Smap{}
	smap.Put("1int", 20010001)
    smap.Put("2string", 1)
    smap.Put("3string",  "[19,1.02,19]")
    smap.Put("4interface", -1)
    smap.Put("5smap", 20010002)
	smap.Put("6interfaceSmap", 10010003)
	
	s1 := ToSortedMapJson(smap)
    fmt.Println(s1)

}

type Smap []*SortMapNode

type SortMapNode struct {
	Key string
	Val interface{}
}

func (c *Smap) Put(key string, val interface{}) {
	index, _, ok := c.get(key)
	if ok {
		(*c)[index].Val = val
	} else {
		node := &SortMapNode{Key: key, Val: val}
		*c = append(*c, node)
	}
}

func (c *Smap) Get(key string) (interface{}, bool) {
	_, val, ok := c.get(key)
	return val, ok
}

func (c *Smap) get(key string) (int, interface{}, bool) {
	for index, node := range *c {
		if node.Key == key {
			return index, node.Val, true
		}
	}
	return -1, nil, false
}

func ToSortedMapJson(smap *Smap) string {
	s := "{"
	for _, node := range *smap {
		v := node.Val
		isSamp := false
		str := ""
		switch v.(type) {
		case *Smap:
			isSamp = true
			str = ToSortedMapJson(v.(*Smap))
		}

		if !isSamp {
			b, _ := json.Marshal(node.Val)
			str = string(b)
		}

		s = fmt.Sprintf("%s\"%s\":%s,", s, node.Key, str)
	}
	s = strings.TrimRight(s, ",")
	s = fmt.Sprintf("%s}", s)
	return s
}
