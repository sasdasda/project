package scene

type TreasureboxList struct {
	Treasurebox []*Treasurebox
}

type Treasurebox struct { 
	Id	int   //  唯一标识  
	ArticleID	int   //  道具ID  
	AwardID	int   //  奖励  
}

