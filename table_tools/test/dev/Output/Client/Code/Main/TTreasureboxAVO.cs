
using ETModel;

namespace ETModel
{
	[Config((int)(AppType.ClientH))]
	public partial class TTreasureboxAVOCategory : ACategory<TTreasureboxAVO>
	{
	}
	
	public class TTreasureboxAVO: IConfig 
	{ 
		/// <summary>唯一标识</summary>
		public int Id { get; set; }
		
		/// <summary>道具ID</summary>
		public int ArticleID;
		
		/// <summary>奖励</summary>
		public int AwardID;
		
	}
	


}
